# Turning the Quality of your development pipeline into a team task

Who was fising the failing pipeline steps?

"Someone must be fixing the API tests?"

Someone was no one.

The "Someone Else's Problem Field" - SEP. q.v. Life, the Universe and Everything. 

See a problem, say a problem. Is it what you should do?

Do I trust developers to take ownership? (I should)

If you have to make someone takes ownership, people don't take ownership if they aren't told to.

Remove: 
Things you don't want to see. - flakey steps, long running steps, complicated to fix steps

Things you don't expect to see.  - Unresponsive external services, New Unanncounced steps, being the nth person to be notified of the failed step

Things you can't explain - Steps with unclear purpose, Steps with unclear failure implications, steps with unclear fix deadlines.

Clear expectations. 
Clear responsibilities, i.e. if you broke it, fix it.
Clear rules. 

Be proactive and it saves you time later. 
But you can do it incrementally, build support from above and below. 
Accept there will be failures!

