# A Code Challenge of Confidence

Firefly shirt, I totally approve. 

What scares you most nowadays

Well it's talking in public. 

This talk has touched me more than any other. I do the same thing. I have, as we all do, impostor syndrome. 

People ask me about Python and Bash and other things, but all I know is what I've learnt in my bumbling journey. 

Talking is me showing myself that I actually know something. 

Sorry dear readers, my notes went off piste there. 

Learning to code increases your empathy for your developers. Programming is very exciting, stopping to test can feel constraining. 
