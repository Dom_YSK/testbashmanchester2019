 # Humble Inquiry

 Ask questions. Don't tell. 

 "We must become better at asking questions instead of telling in a culture that values telling over questioning."

As the fundamental question: What do information do you want from this?

It sounds a lot like the Agile feedback cycle. 

Do people even know what they want? 

Questioning is onky half the issue, Listening and realising how to use that to inforn your further questioning. 

Working in isolation is a good way to find out that people don't like to have things handed to them by fiat

Everyone is different, some people can visualise things very easily. Some people can't form images in their mind. I am in the former camp, I don't understand how the latter camp can think or dream. 
