# Failure is not an option, it's mandatory. 

If failure is not an option it makes the scenario toxic.

Every failure is a step towards success. - Miss Frizzle. 

Version 1 is never the optimum, define minimum viable product.

I spent all morning failing to tweet, eventually I realised that my approach from Linux
won't work on a mac and despite my talk tomorrow on Bash, Python was the answer. I love Python.

Celebrate your failure, you've learnt something. Though in my case it just confirmed that I prefer Linux to Macos

Failure does not mean you can't succeed. The faster you fail, the faster you're learning. 

Swear and celebrate, why not? 
