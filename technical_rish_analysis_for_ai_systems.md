# Technical Risk Analysis for AI systems.

AI is often conflated with machine learning now. 

Understanding the rules of recognising people, for example, is extremely hard to foramlise. Machine learning is hard to actually define. 

It can be used inadvisably, for example in hiring.

Machine learning is a continual process, It learns and refines, often. Though you can use a classifier without back propagation, if you want to use it in an edge computing device. For example you can train a model to recognise garden birds and then push the model to a raspberry pi to count and categorise the birds in your garden. Use a real GPU to train it, honestly you'll thank yourself. Make sure you don't overfit, sprinkle in lots of negatives too. 

 Select data to give you the most variability so that you don't bias the model. 

But is more data always good?

Watch out for people poisoning the dataset, see Tae bot.

Learn about adversarial techniques. Dazzle makeup can confuse machine learning. IR LEDS on glasses can also completely blind cameras.

Systemic errors, can be indicative of a bad training set or poor model. 
